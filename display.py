#!/usr/bin/env python3
# Copyright (C) 2018 Clayton Craft <clayton@craftyguy.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import pyglet
from pyglet.window import key

pyglet.options['debug_gl'] = False

key_map = {key._1: 0x1, key._2: 0x2, key._3: 0x3, key._4: 0xC,
           key.Q:  0x4, key.W:  0x5, key.E:  0x6, key.R:  0xD,
           key.A:  0x7, key.S:  0x8, key.D:  0x9, key.F:  0xE,
           key.Z:  0xA, key.X:  0x0, key.C:  0xB, key.V:  0xF}


class display(pyglet.window.Window):
    def __init__(self, debug, foreground_color, background_color,
                 fullscreen=False):
        # Note: vsync really slows down the emulator due to the cycle
        # method being fired with pyglet's 'schedule'
        super().__init__(fullscreen=fullscreen, vsync=False)
        self.fg = foreground_color
        self.bg = background_color
        self.gfx = None
        self.pixel_w = int(self.width / 64)
        self.pixel_h = int(self.height / 32)
        self.debug = debug
        self.keys = [0] * 16
        self.scheduled_freq = 1
        # Sound 'beep' loading and loop initialization
        beep = pyglet.media.procedural.Triangle(1.0, frequency=267)
        looper = pyglet.media.SourceGroup(beep.audio_format, None)
        looper.loop = True
        looper.queue(beep)
        self.beep = pyglet.media.Player()
        self.beep.queue(looper)
        self.beeping = False
        # Create 'pixel' image
        pattern = pyglet.image.SolidColorImagePattern(color=self.fg)
        self.pixel = pattern.create_image(self.pixel_w, self.pixel_h)
        # Generate all 2048 sprites ahead of time
        self.batch = pyglet.graphics.Batch()
        self.sprites = [self.get_sprite(i) for i in range(0, 2048)]
        # glClearColor is used to set background color
        # glClearColor parameters are in form R, G, B, A and clamped to (0, 1)
        pyglet.gl.glClearColor(self.bg[0]/255.0, self.bg[1]/255.0,
                               self.bg[2]/255.0, 1)

    def schedule(self, method, freq):
        """
        Stops scheduled method (if any running) and starts it again
        """
        self.scheduled_method = method
        self.scheduled_freq = freq
        print('Frequency set to:\t%iHz' % freq)
        pyglet.clock.unschedule(self.scheduled_method)
        pyglet.clock.schedule_interval(self.scheduled_method,
                                       1 / self.scheduled_freq)

    def run(self):
        pyglet.app.run()

    def update(self, gfx):
        self.gfx = gfx

    def beep_on(self):
        self.beep.play()
        self.beeping = True

    def beep_off(self):
        self.beep.pause()
        self.beeping = False

    # TODO: Fix this
    # def on_resize(self, width, height):
    #    self.pixel_w = int(self.width / 64)
    #    self.pixel_h = int(self.height / 32)
    #    self.sprites = [self.get_sprite(i) for i in range(0, 2048)]

    def on_draw(self):
        if not self.gfx:
            return
        self.clear()
        debug = self.debug
        i = 0
        for p in self.gfx:
            o = 255 * p
            # pixel sprites are toggled on/off by changing opacity
            # only update sprites that have been toggled in this cycle
            if self.sprites[i].opacity != o:
                self.sprites[i].opacity = o
            if 'gfx' in debug and p:
                print('x: %i\ty: %i\tpixel_w: %i\tpixel_h: %i\tindex:%i'
                      % (self.sprites[i].x, self.sprites[i].y,
                         self.pixel_w, self.pixel_h, i))
            i += 1
        self.batch.draw()

    def get_sprite(self, i):
        """
        Returns a sprite of a pixel at gfx[i]
        """
        x = ((i % 64) * self.pixel_w)
        # pyglet's coordinate system has 0,0 at bottom left
        y = (self.height - (int(i / 64) * self.pixel_h)) - self.pixel_h
        return pyglet.sprite.Sprite(self.pixel, x=x, y=y, batch=self.batch)

    def on_key_press(self, sym, mod):
        if sym == key.ESCAPE:
            pyglet.app.event_loop.exit()
        if sym in key_map:
            self.keys[key_map[sym]] = 1
        if sym == key.PLUS:
            self.scheduled_freq *= 1.2
            self.schedule(self.scheduled_method, self.scheduled_freq)
        if sym == key.MINUS:
            self.scheduled_freq *= .8
            self.schedule(self.scheduled_method, self.scheduled_freq)

    def on_key_release(self, sym, mod):
        if sym in key_map:
            self.keys[key_map[sym]] = 0
