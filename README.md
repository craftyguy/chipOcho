# chipOcho

This is a Chip 8 emulator written in Python 3, for fun and (no) profit. It's mostly working now!

I recommend running `main.py --help` to see the latest options available. Currently there are options to:
- Enable debug messages for certain components of the system
- Enable various (two) Chip 8 quirks
- Set foreground and background colors
- Run fullscreen!

### Requirements
- Python 3.6
- python-levenshtein
- pyglet


### Keybindings

```
Chip 8 Keypad            Keyboard
+-+-+-+-+                +-+-+-+-+
|1|2|3|C|                |1|2|3|4|
+-+-+-+-+                +-+-+-+-+
|4|5|6|D|                |Q|W|E|R|
+-+-+-+-+       =>       +-+-+-+-+
|7|8|9|E|                |A|S|D|F|
+-+-+-+-+                +-+-+-+-+
|A|0|B|F|                |Z|X|C|V|
+-+-+-+-+                +-+-+-+-+
```

| Key | Function           
|----:|:-------------------------| 
|`-`  | Decrease emulator speed  | 
| `+` | Increase emulator speed  |


### References

 - [How to write an emulator (CHIP-8 interpreter)](http://www.multigesture.net/articles/how-to-write-an-emulator-chip-8-interpreter/)
 
 - [Mastering Chip 8](http://mattmik.com/files/chip8/mastering/chip8.html)
 
 - [Wikipedia - Chip 8](https://en.wikipedia.org/wiki/CHIP-8)
