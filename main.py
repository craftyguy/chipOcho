#!/usr/bin/env python3
# Copyright (C) 2018 Clayton Craft <clayton@craftyguy.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import argparse
import chipOcho
import os
import re
import sys

colors = {
            'green':    (255, 0, 255, 0),
            'red':      (255, 255, 0, 0),
            'blue':     (255, 0, 0, 255),
            'purple':   (255, 255, 0, 255),
            'black':    (255, 0, 0, 0),
            'white':    (255, 255, 255, 255)
        }


def main():
    debug = ['cpu', 'keys', 'gfx', 'all']
    quirks = ['loadstore', 'shift', 'all']
    ap = argparse.ArgumentParser()
    ap.add_argument('-g', '--game-file', type=str,
                    help='Game file to load.',
                    required=True)
    ap.add_argument('-f', '--foreground-color',
                    help='Foreground color in format 0xRRGGBB',
                    type=str, default='')
    ap.add_argument('-b', '--background-color',
                    help='Background color in format 0xRRGGBB',
                    type=str, default='')
    ap.add_argument('-s', '--fullscreen',
                    help='Run in fullscreen mode.',
                    action='store_true')
    ap.add_argument('-q', '--quirks', nargs='+',
                    choices=quirks, default=[],
                    help='Enable chip 8 quirks.')
    ap.add_argument('-d', '--debug', nargs='+',
                    choices=debug, default=[],
                    help='Print debug messages for associated components. ')

    args = ap.parse_args()
    if not os.path.exists(args.game_file):
        assert False, ("ERROR: Unable to locate game file: %s"
                       % args.game_file)

    hex_color_re = r'^0x(?:[0-9a-fA-F]{3}){1,2}$'
    if re.search(hex_color_re, args.background_color):
        bg = int(args.background_color, 16)
        bg = ((bg & 0xFF0000) >> 16, (bg & 0x00FF00) >> 8, bg & 0x0000FF, 255)
    else:
        # default color
        bg = (0, 0, 0, 0)
    if re.search(hex_color_re, args.foreground_color):
        fg = int(args.foreground_color, 16)
        fg = ((fg & 0xFF0000) >> 16, (fg & 0x00FF00) >> 8, fg & 0x0000FF, 255)
    else:
        fg = (255, 0, 255, 255)

    if 'all' in args.debug:
        debug.remove('all')
    else:
        debug = args.debug

    if 'all' in args.quirks:
        quirks.remove('all')
    else:
        quirks = args.quirks

    chip8 = chipOcho.chipOcho(debug=debug,
                              fullscreen=args.fullscreen,
                              foreground_color=fg,
                              background_color=bg)
    for quirk in quirks:
        if quirk in chip8.quirks:
            chip8.quirks[quirk] = True
    chip8.load_game(args.game_file)
    sys.exit(chip8.run())


if __name__ == "__main__":
    main()
