#!/usr/bin/env python3
# Copyright (C) 2018 Clayton Craft <clayton@craftyguy.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import display
import math
import Levenshtein
import random


class chipOcho:
    quirks = {'loadstore': False,
              'shift': False}

    def __init__(self, debug, foreground_color, background_color,
                 fullscreen=False):
        print("Welcome to Chip Ocho")
        self.debug = debug
        self.freq = 500         # limit execution to 500Hz
        # Note: timers are floats, and rounded up when accessed by programs
        # This is to allow scaling the timer tick according to chip frequency,
        # since the timer on chip8 runs at 60Hz
        self.timer_inc = 60.0 / self.freq
        self.delay_timer = 0.0
        self.snd_timer = 0.0
        self.memory = bytearray(4096)
        self.opcode = 0
        self.pc = 512           # program counter
        self.i = 0              # address register
        self.v = [0] * 16       # CPU registers (2 bytes each, 16 total)
        self.stack = []
        self.draw_flag = False
        self.gfx = [0] * (32 * 64)
        self.opcodes = {'0NNN': self.op_0NNN, '00E0': self.op_00E0,
                        '00EE': self.op_00EE, '1NNN': self.op_1NNN,
                        '2NNN': self.op_2NNN, '3XNN': self.op_3XNN,
                        '4XNN': self.op_4XNN, '5XY0': self.op_5XY0,
                        '6XNN': self.op_6XNN, '7XNN': self.op_7XNN,
                        '8XY0': self.op_8XY0, '8XY1': self.op_8XY1,
                        '8XY2': self.op_8XY2, '8XY3': self.op_8XY3,
                        '8XY4': self.op_8XY4, '8XY5': self.op_8XY5,
                        '8XY6': self.op_8XY6, '8XY7': self.op_8XY7,
                        '8XYE': self.op_8XYE, '9XY0': self.op_9XY0,
                        'ANNN': self.op_ANNN, 'BNNN': self.op_BNNN,
                        'CXNN': self.op_CXNN, 'DXYN': self.op_DXYN,
                        'EX9E': self.op_EX9E, 'EXA1': self.op_EXA1,
                        'FX07': self.op_FX07, 'FX0A': self.op_FX0A,
                        'FX15': self.op_FX15, 'FX18': self.op_FX18,
                        'FX1E': self.op_FX1E, 'FX29': self.op_FX29,
                        'FX33': self.op_FX33, 'FX55': self.op_FX55,
                        'FX65': self.op_FX65}
        # This helps the decoder execution time:
        self.opcode_names = self.opcodes.keys()
        self.font_set = [0xF0, 0x90, 0x90, 0x90, 0xF0,  # 0
                         0x20, 0x60, 0x20, 0x20, 0x70,  # 1
                         0xF0, 0x10, 0xF0, 0x80, 0xF0,  # 2
                         0xF0, 0x10, 0xF0, 0x10, 0xF0,  # 3
                         0x90, 0x90, 0xF0, 0x10, 0x10,  # 4
                         0xF0, 0x80, 0xF0, 0x10, 0xF0,  # 5
                         0xF0, 0x80, 0xF0, 0x90, 0xF0,  # 6
                         0xF0, 0x10, 0x20, 0x40, 0x40,  # 7
                         0xF0, 0x90, 0xF0, 0x90, 0xF0,  # 8
                         0xF0, 0x90, 0xF0, 0x10, 0xF0,  # 9
                         0xF0, 0x90, 0xF0, 0x90, 0x90,  # A
                         0xE0, 0x90, 0xE0, 0x90, 0xE0,  # B
                         0xF0, 0x80, 0x80, 0x80, 0xF0,  # C
                         0xE0, 0x90, 0x90, 0x90, 0xE0,  # D
                         0xF0, 0x80, 0xF0, 0x80, 0xF0,  # E
                         0xF0, 0x80, 0xF0, 0x80, 0x80]  # F
        # font set is loaded in first 80 bytes
        for c in range(0, len(self.font_set)):
            self.memory[c] = self.font_set[c]
        self.display = display.display(debug=self.debug,
                                       foreground_color=foreground_color,
                                       background_color=background_color,
                                       fullscreen=fullscreen)

    def run(self):
        print("Enabled quirks:")
        for q in self.quirks.keys():
            if self.quirks[q]:
                print('  - %s' % q)
        self.display.schedule(self.cycle, self.freq)
        self.display.run()

    def cycle(self, nope):
        """
        cycle performs:
            - get keys
            - fetch opcode
            - decode opcode
            - execute opcode
            - timers updated
            - update display
        """
        if 'cpu' in self.debug:
            print('\n')
            print('v: ' + ' '.join(['%01X:%02X' % (x, self.v[x])
                                    for x in range(0, 8)]))
            print('v: ' + ' '.join(['%01X:%02X' % (x, self.v[x])
                                    for x in range(8, 16)]))
            print('delay_timer: ' + str(math.ceil(self.delay_timer)))
            print('sound_timer: ' + str(math.ceil(self.snd_timer)))
            print('draw_flag: ' + str(self.draw_flag))
            print('opcode: %04X\ti: %04X\tpc: %04X'
                  % (self.opcode, self.i, self.pc))
            print('call stack (most recent last): '
                  + ', '.join(['0x%03X' % (x) for x in self.stack]))
        if 'keys' in self.debug:
            print('keys: ' + ','.join(map(str, self.display.keys)))

        self.fetch_opcode()
        opcode_method = self.decode_opcode()
        if opcode_method:
            self.opcodes[opcode_method]()
        else:
            assert False, "ERROR: Unknown opcode: %s" % self.opcode
        # count down timers
        if self.delay_timer > 0:
            self.delay_timer -= self.timer_inc
        if self.snd_timer > 0:
            self.snd_timer -= self.timer_inc
            if not self.display.beeping:
                self.display.beep_on()
        else:
            if self.display.beeping:
                self.display.beep_off()
        if self.draw_flag:
            self.display.update(self.gfx)
            self.draw_flag = False

    def fetch_opcode(self):
        self.opcode = (self.memory[self.pc] << 8)
        self.opcode |= self.memory[self.pc + 1]

    def decode_opcode(self):
        """
        returns the function in self.opcodes with a key that is most
        like self.opcode.
        If opcode is not found (or is 0), return NoneType
        """
        if not self.opcode:
            return None
        # convert opcode to string, padded with 0s
        opcode_str = '%04X' % self.opcode
        # levenshtein score, lower = better
        best_score = 99
        best_opcode = None
        score = 0
        for key in self.opcode_names:
            if key[0] == opcode_str[0]:
                score = Levenshtein.distance(opcode_str, key)
                if score < best_score:
                    best_opcode = key
                    best_score = score
        return best_opcode

    def load_game(self, game_file):
        """
        Load game file into given memory
        """
        buf = bytearray()
        with open(game_file, 'rb') as fd:
            buf = fd.read()
            if len(buf) > len(self.memory):
                assert False, ("ERROR: The given game file is too large to "
                               "load into emulator memory!")
            # Note: programs loaded at 0x200 (512)
            for i in range(0, len(buf)):
                self.memory[512 + i] = buf[i]

    def dump_memory(self, line_max=16):
        """
        Dump entire contents of self.memory
        """
        row = ''
        mem_len = len(self.memory)
        for i in range(0, mem_len):
            cell = '0x%02X' % self.memory[i]
            # start of row:
            if i % line_max == 0:
                row += '0x%03X| %s' % (i, cell)
            row += ' %s' % cell
            if i % line_max == (line_max - 1):
                print(row)
                row = ''

    def op_0NNN(self):
        self.pc += 2
        pass

    def op_00E0(self):
        self.gfx = [0] * 2048
        self.pc += 2

    def op_00EE(self):
        self.pc = self.stack.pop()
        self.pc += 2

    def op_1NNN(self):
        self.pc = self.opcode & 0x0FFF

    def op_2NNN(self):
        self.stack.append(self.pc)
        self.pc = self.opcode & 0x0FFF

    def op_3XNN(self):
        x = (self.opcode & 0x0F00) >> 8
        val = self.opcode & 0x00FF
        if self.v[x] == val:
            self.pc += 4
        else:
            self.pc += 2

    def op_4XNN(self):
        x = (self.opcode & 0x0F00) >> 8
        val = self.opcode & 0x00FF
        if self.v[x] != val:
            self.pc += 4
        else:
            self.pc += 2

    def op_5XY0(self):
        x = (self.opcode & 0x0F00) >> 8
        y = (self.opcode & 0x00F0) >> 4
        if self.v[x] == self.v[y]:
            self.pc += 4
        else:
            self.pc += 2

    def op_6XNN(self):
        x = (self.opcode & 0x0F00) >> 8
        val = self.opcode & 0x00FF
        self.v[x] = val
        self.pc += 2

    def op_7XNN(self):
        x = (self.opcode & 0x0F00) >> 8
        val = self.opcode & 0x00FF
        self.v[x] += val
        # 'simulate' overflow
        self.v[x] &= 0x0FF
        self.pc += 2

    def op_8XY0(self):
        x = (self.opcode & 0x0F00) >> 8
        y = (self.opcode & 0x00F0) >> 4
        self.v[x] = self.v[y]
        self.pc += 2

    def op_8XY1(self):
        x = (self.opcode & 0x0F00) >> 8
        y = (self.opcode & 0x00F0) >> 4
        self.v[x] = self.v[x] | self.v[y]
        self.pc += 2

    def op_8XY2(self):
        x = (self.opcode & 0x0F00) >> 8
        y = (self.opcode & 0x00F0) >> 4
        self.v[x] = self.v[x] & self.v[y]
        self.pc += 2

    def op_8XY3(self):
        x = (self.opcode & 0x0F00) >> 8
        y = (self.opcode & 0x00F0) >> 4
        self.v[x] = self.v[x] ^ self.v[y]
        self.pc += 2

    def op_8XY4(self):
        x = (self.opcode & 0x0F00) >> 8
        y = (self.opcode & 0x00F0) >> 4
        self.v[0xF] = 0
        s = self.v[x] + self.v[y]
        if s > 255:
            s -= 255
            self.v[0xF] = 1
        self.v[x] = s
        self.pc += 2

    def op_8XY5(self):
        x = (self.opcode & 0x0F00) >> 8
        y = (self.opcode & 0x00F0) >> 4
        self.v[0xF] = 1
        d = self.v[x] - self.v[y]
        if d < 0:
            d += 255
            self.v[0xF] = 0
        self.v[x] = d
        self.pc += 2

    def op_8XY6(self):
        x = (self.opcode & 0x0F00) >> 8
        y = (self.opcode & 0x00F0) >> 4
        self.v[0xF] = self.v[y] & 1
        self.v[x] = self.v[y] >> 1
        if not self.quirks['shift']:
            self.v[y] = self.v[x]
        self.pc += 2

    def op_8XY7(self):
        x = (self.opcode & 0x0F00) >> 8
        y = (self.opcode & 0x00F0) >> 4
        self.v[0xF] = 1
        d = self.v[y] - self.v[x]
        if d < 0:
            d += 255
            self.v[0xF] = 0
        self.v[x] = d
        self.pc += 2

    def op_8XYE(self):
        x = (self.opcode & 0x0F00) >> 8
        y = (self.opcode & 0x00F0) >> 4
        # save most significant bit
        self.v[0xF] = (self.v[y] & 0x80) >> 7
        # shift left and mask out > 0xFF
        self.v[x] = (self.v[y] << 1) & 0x0FF
        if not self.quirks['shift']:
            self.v[y] = self.v[x]
        self.pc += 2

    def op_9XY0(self):
        x = (self.opcode & 0x0F00) >> 8
        y = (self.opcode & 0x00F0) >> 4
        if self.v[x] != self.v[y]:
            self.pc += 4
        else:
            self.pc += 2

    def op_ANNN(self):
        self.i = self.opcode & 0x0FFF
        self.pc += 2

    def op_BNNN(self):
        val = self.opcode & 0x0FFF
        self.pc = self.v[0x0] + val

    def op_CXNN(self):
        x = (self.opcode & 0x0F00) >> 8
        val = self.opcode & 0x00FF
        self.v[x] = random.randrange(0, 256) & val
        self.pc += 2

    def op_DXYN(self):
        x = self.v[(self.opcode & 0x0F00) >> 8]
        y = self.v[(self.opcode & 0x00F0) >> 4]
        self.draw_flag = True
        height = self.opcode & 0x000F
        width = 8
        self.v[0xF] = 0
        for yl in range(0, height):
            pixel = self.memory[self.i + yl]
            for xl in range(0, width):
                # compare every bit in pixel to see if it is set
                if pixel & (0x80 >> xl) != 0:
                    i = x + xl + ((y + yl) * 64)
                    if i > 2047:
                        i = 2047
                    if self.gfx[i] == 1:
                        self.v[0xF] = 1
                    self.gfx[i] ^= 1
        self.pc += 2

    def op_EX9E(self):
        x = (self.opcode & 0x0F00) >> 8
        if self.display.keys[self.v[x]]:
            self.pc += 4
        else:
            self.pc += 2

    def op_EXA1(self):
        x = (self.opcode & 0x0F00) >> 8
        if not self.display.keys[self.v[x]]:
            self.pc += 4
        else:
            self.pc += 2

    def op_FX07(self):
        x = (self.opcode & 0x0F00) >> 8
        self.v[x] = math.ceil(self.delay_timer)
        self.pc += 2

    def op_FX0A(self):
        x = (self.opcode & 0x0F00) >> 8
        for i in self.display.keys:
            if self.display.keys[i]:
                self.v[x] = i
                self.pc += 2
                break
        # pc is NOT incremented if no key press since
        # this is a blocking call

    def op_FX15(self):
        x = (self.opcode & 0x0F00) >> 8
        self.delay_timer = self.v[x]
        self.pc += 2

    def op_FX18(self):
        x = (self.opcode & 0x0F00) >> 8
        self.snd_timer = self.v[x]
        self.pc += 2

    def op_FX1E(self):
        x = (self.opcode & 0x0F00) >> 8
        self.i += self.v[x]
        self.v[0xF] = 1 if self.i > 0xFFF else 0
        self.pc += 2

    def op_FX29(self):
        x = (self.opcode & 0x0F00) >> 8
        # fonts are 5 bytes
        self.i = self.v[x] * 5
        self.pc += 2

    def op_FX33(self):
        x = (self.opcode & 0x0F00) >> 8
        self.memory[self.i] = int(self.v[x] / 100)
        self.memory[self.i + 1] = int((self.v[x] / 10) % 10)
        self.memory[self.i + 2] = int((self.v[x] % 100) % 10)
        self.pc += 2

    def op_FX55(self):
        x = (self.opcode & 0x0F00) >> 8
        offset = self.i
        quirk = self.quirks['loadstore']
        for a in range(0, x + 1):
            self.memory[offset + a] = self.v[a]
            if not quirk:
                self.i += 1
        self.pc += 2

    def op_FX65(self):
        x = (self.opcode & 0x0F00) >> 8
        quirk = self.quirks['loadstore']
        for a in range(0, x + 1):
            self.v[a] = self.memory[self.i]
            if not quirk:
                self.i += 1
        self.pc += 2
